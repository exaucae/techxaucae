# see https://www.systutorials.com/how-to-get-the-full-path-and-directory-of-a-makefile-itself/
# and https://stackoverflow.com/a/23324703/13781069
ROOT_DIR:= $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

DEST_DIR := ${ROOT_DIR}/public
SRC_DIR := ${ROOT_DIR}/tech


.DEFAULT_GOAL := start
.PHONY:=  all  clean 



all: build start 


start: check
		hugo server --source ${SRC_DIR} \
		--destination ${DEST_DIR} \
		--buildDrafts \
		--verbose \
		--watch \
		--ignoreCache \
		--i18n-warnings
	
build:  install
		hugo --source ${SRC_DIR}  \
		--destination ${DEST_DIR} \
		--gc \
		--i18n-warnings \
		--verbose  

check: 
		hugo check --source ${SRC_DIR} \
		--verbose

install:
		curl -L https://github.com/gohugoio/hugo/releases/download/v0.89.4/hugo_extended_0.89.4_Linux-64bit.tar.gz  | tar xvz hugo
		mv hugo /usr/local/bin/hugo		

clean:
		rm -rvf ${DEST_DIR}
