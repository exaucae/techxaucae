---
title: "Clean Architecture for all of us"
date: 2021-06-14T15:33:46Z
slug: "clean-architecture"
type: new
draft: false
toc: true
cover: /img/clean-archi/explicit-architecture.png 
coverCaption: A detailed view of hexagonal architecture  
tags:
- architecture
- hexagonal
- backend

categories:
- architecture

---

## Introduction

Post on migration. Available here: <https://www.linkedin.com/pulse/clean-architecture-all-us-chrys-exaucet-perceveran-ngoma/>
