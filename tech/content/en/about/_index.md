---
title: "About"
date: 2021-06-14T18:33:46Z
draft: false
---
Chrys Exaucet is a result-driven minimalist Software Architect from Congo. Craftsman at heart, he has engineered robust software for decentralized identity, logistics, finance, and healthcare products, both on-premises and in the cloud for 3+ years. He also writes about the JVM ecosystem on [baeldung](https://www.baeldung.com/author/chrysexaucet). Besides, Chrys sings in a choir since 2011.
