---
title: "Consultancy"
date: 2024-03-20
draft: false
---
**Services**
- **Cloud Architecture Design**: Tailored cloud architecture solutions optimized for performance, scalability, and cost-effectiveness.
- **Infrastructure as Code (IaC)**: Implementing Infrastructure as Code using tools like Terraform, Bicep, CDK, CloudFormation.
- **Cloud Migration Strategy**: Seamless migration of on-premises infrastructure to the cloud.
- **AWS/Azure Best Practices**: Expert advice on leveraging AWS and Azure services effectively.
- **Remote Consulting**: Flexibility and accessibility with remote consultancy services around general software development.

**Let's talk**
- Email: [chrysexaucet@hotmail.fr](mailto:chrysexaucet@hotmail.fr)
- LinkedIn: https://www.linkedin.com/in/chrys-exaucet
